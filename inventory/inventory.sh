#!/bin/bash

# Copyright (C) 2018  Israel Felipe Prates
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

################################################################################
#                                 Declarations                                 #
################################################################################

# Read-only variable with a regex that matches any lines with one or more pairs
# of double quotes followed by a semicolon.
declare -r pattern='^(("[^";]*")+;)+$'
# Source name.
declare -r source='./file-01.csv'
# Temporary file. Semicolons removed from field contents.
declare -r filtered="$(mktemp filtered-temp-XXXXXX)"
# Temporary file. Locations (5th column).
declare -r locations="$(mktemp locations-temp-XXXXXX)"
# Output directory.
declare -r output_dir='./output/'

################################################################################
#                             Function definitions                             #
################################################################################

# Function to print an error message to stderr.
function print_error() {
	printf "%s\n" "$*" >&2
}

# Clean-up function. Silently delete any file(s) passed as argument.
function clean() {
    rm -f "$@"
}

################################################################################
#                                Start of script                               #
################################################################################

# Define the clean-up function as the handler to the exit event.
trap 'clean ${filtered} ${locations}' EXIT

# Check whether the output directory exists. Abort if doesn't.
if [ ! -d "${output_dir}" ] || [ ! -f "${source}" ]; then
	print_error "${0}: Error: Could not locate necessary files. Aborting."
	exit 1
fi

grep -E "${pattern}" "${source}" > "${filtered}"

cut -d';' -f5 "${filtered}" | sort -u > "${locations}"

# Run on a subshell in order to preserve IFS (internal field separator).
(
	IFS=''
	while read -r line || [[ -n "${line}" ]]; do
		grep -E "${line};$" "${source}" | sort > "${output_dir}${line//\"/}.csv"
	done < "${locations}"
)

# EOF
