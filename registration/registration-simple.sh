#!/bin/bash

# Copyright (C) 2018  Israel Felipe Prates
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

################################################################################
#                                 Declarations                                 #
################################################################################

# Path to the data directory.
declare -r data_directory='./registration_data/'
# Pattern that describes the data files' extension.
declare -r extension_pattern='*.dat'
# Path to the output file.
declare -r output='./output.dat'
# Dictionary holding the paths to the semester temporary files.
# Indexed by semester.
declare -A temp_semesters=()

################################################################################
#                             Function definitions                             #
################################################################################

# Function to print an error message to stderr.
function print_error() {
	printf "%s\n" "$*" >&2
}

# Clean-up function. Silently delete any file(s) passed as argument.
function clean() {
	rm -f "$@"
}

################################################################################
#                                Start of script                               #
################################################################################

# Check whether the data directory exists. If it does not, exit with failure.
if [ ! -d "${data_directory}" ]; then
	print_error "${0}: Error: could not find '${data_directory}' directory."
	exit 1
fi

# Trap the exit signal and hook the clean function to it.
# Its arguments are the contents of the temp_semesters dictionary, expanded only
# when the signal is received (note the quotes).
trap 'clean ${temp_semesters[@]}' EXIT

# Iterate through the files under the data directory whose extensions match the
# extension_pattern.
for file in $(find "${data_directory}" -name "${extension_pattern}"); do
	# Remove, starting from the front, anything until the last slash, inclusive.
	semester="${file##*/}"
	# Remove, starting from the back, anything until the last dot, inclusive.
	semester="${semester%%.*}"
	temp="${semester}.tmp"
	# Use the semester as key in the dictionary to store the temp file path.
	temp_semesters["${semester}"]="${temp}"
	# Exclude the first line by indicating to head that the second line from
	# the file should be used as starting point.
	# Get the second column from the piped output and append it to the file
	# specified by temp. The second column is the GRR. The separator is the
	# colon character (:).
	tail --lines=+2 "${file}" | cut -d':' -f2 >> "${temp}"
done

# Truncate (empty) the output file.
> "${output}"

# Iterate through the  keys of the temp_semesters dictionary.
for semester in "${!temp_semesters[@]}"; do
	# Echo the semester and the amount of unique registrations in it. Append
	# the result to the output file.
	echo "${semester}:$(sort -u ${temp_semesters[${semester}]} | wc -l)"
		>> "${output}"
done

# Sort (in place) the output lines.
sort -n --output="${output}" "${output}"
# EOF
