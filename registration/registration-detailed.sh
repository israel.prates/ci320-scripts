#!/bin/bash

# Copyright (C) 2018  Israel Felipe Prates
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

################################################################################
#                                 Declarations                                 #
################################################################################

declare -r root='./registration_data/'

# Command line options.
declare -r options=$(getopt -o hp -l header pretty-print "${0}" -- "$@")
# Underlying flag for options.
declare header=false
declare pretty_print=false

# Parse command line options.
while true; do
	case "${1}" in
		-h | --header ) header=true; shift ;; # Header option.
		-p | --pretty-print ) pretty_print=true; shift ;; # Pretty print option.
		-- ) shift; break ;; # Last option has been read. Break.
		* ) break ;; # Anything else. Break.
	esac
done

################################################################################
#                             Function definitions                             #
################################################################################

# Function to print an error message to stderr.
function print_error() {
	printf "%s\n" "$*" >&2
}

# Function to perform a specified function on a stream.
function map_function() {
	local -r mapped_function="${1}"
	while read line; do
		# Call the function with the line as its argument.
		"${mapped_function}" "${line}"
	done
}

# Function to subtract one from its argument and print the result.
function minus_one() {
	local -ri value="${1}"
	printf "%d\n" "$((${value} - 1))"
}

# Count registrations for all disciplines.
function count_registrations() {
	local -r dir="${1}"
	pushd "${dir}" > /dev/null # Go down into dir.
	printf "%s" "$(grep -c '.*' * | # Count matches on each file.
		cut -d':' -f 2 | # Get the second column, which is the count itself.
		map_function minus_one | # Subtract one from each line.
		tr '\n' ' ' | # Translate line feeds to spaces.
		sed 's/ $//')" # Remove trailing space.
	popd > /dev/null # Go up into dir structure.
}

# Function to create the header, where each column is a semester.
function compose_header() {
	local dir="${1}"
	local ext='*.dat'
	for item in $(find "${dir}"/* -name "${ext}"); do
		item="${item##*/}"
		printf "%s\n" "${item%%.*}"
	done | sort -u | tr '\n' ' ' | sed 's/ $//'
}

################################################################################
#                                Start of script                               #
################################################################################

# Check whether the root directory exists. If not, then exit with failure..
if [ ! -d "${root}" ]; then
	print_error "${0}: Error: could not find '${root}' directory."
	exit 1
fi

# Subshell. Used for redirection ahead in the script.
(
	# Print the header if the corresponding option has been provided.
	if [[ "${header}" = true ]]; then
		printf "Discipline/Semester %s\n" "$(compose_header ${root})"
	fi
	pushd "${root}" > /dev/null # Go into root directory.
	# Count the registrations on each discipline.
	# Each directory is the name of a discipline.
	for dir in $(ls .); do
		printf "${dir} %s\n" "$(count_registrations ${dir})"
	done
	popd > /dev/null
) | (
		# Check whether the pretty_print option has been checked. If true, then
		# use column to format the output, otherwise simply display the result.
		[[ "${pretty_print}" = true ]] && column -t || cat
	)
# EOF
