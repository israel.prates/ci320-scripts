#!/bin/bash

# Copyright (C) 2018  Israel Felipe Prates
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

################################################################################
#                                 Declarations                                 #
################################################################################

# Path to the source file. Compacted with tar and gzip.
declare -r source='/home/c3sl/marcos/tmp/dadosmatricula.tgz'
# Path to the destination file. Compacted with tar and gzip.
declare -r destination='/nobackup/bcc/ifp15/registration-data.tgz'
# Extracted directory path.
declare -r extracted='/nobackup/bcc/ifp15/registration-data/'
# Symbolic link to the extracted directory.
declare -r link_name='./registration-data'

################################################################################
#                                Start of script                               #
################################################################################

# Check whether the source file exists and is readable.
# If not, then print an error message and exit with failure.
[ -f "${source}" ] || {
	echo "${0}: Error: unable to locate source file." >&2
	exit 1
}

# Check whether the extracted directory exists. If it doesn't, then create it.
[ -d "${extracted}" ] || mkdir "${extracted}"

# Create a symbolic link to the extracted directory.
ln -fsT "${extracted}" "${link_name}"

# Copy the source file to the destination path.
cp "${source}" "${destination}"

# Extract the source file to the extracted directory while stripping its root.
tar xf "${destination}" -C "${extracted}" --strip-components 1

# Rename every file whose extension is .dados so it reads .dat instead.
find "${extracted}" -name '*.dados' -exec rename -f 's/\.dados/\.dat/i' {} \;
# EOF
