#!/bin/bash

# Copyright (C) 2018  Israel Felipe Prates
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

declare -r type="${1}"
declare -r occurrences="${2}"

# The file delimiter could be anything, not only EOF.
mail -s 'Abnormal Number of Blocked Access Attempts' 'ifp15@inf.ufpr.br' <<EOF
This message is to notify you that an abnormal number of requests has been blocked on your firewall. Type and number of occurrences below.

${type} ${occurrences}
EOF
