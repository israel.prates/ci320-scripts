#!/usr/bin/awk -f

# Copyright (C) 2018  Israel Felipe Prates
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# Print s in red.
# Messes sorting, since the escape sequence is part of the string.
function red(s) {
	printf "\033[1;31m" s "\033[0m"
}
# Print s in green.
# Messes sorting, since the escape sequence is part of the string.
function green(s) {
	printf "\033[1;32m" s "\033[0m"
}
BEGIN {
	types_list="./firewall-types.txt"
	send_mail_script="./firewall-send-mail.sh"
	threshold=20000

	# Initialize the array.
	while ((getline type < types_list) > 0) {
		occurrences[type]=0
	}
}
{
	# Increment the count of occurrences for a given type.
	occurrences[$6]++
}
END {
	# Loop through the keys.
	for (type in occurrences) {
		# Check for abnormal amount of occurrences
		if (occurrences[type] >= threshold) {
			# Send the notification messages.
			# Setting either of below variables will prevent the messages from
			# being sent.
			if (!suppress_email && !nomsg) {
				system(send_mail_script " " type " " occurrences[type])
			}
			# Report occurrences exceeding the threshold in red.
			print red(type " " occurrences[type])
		}
		else {
			# Report normal occurrences in green.
			print green(type " " occurrences[type])
		}
	}
}
